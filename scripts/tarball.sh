#!/bin/bash

#Generate tar ball

#tar -czvf sample.tar.gz /TEST_FOLDER/sample-dpl/ 

source /TEST_FOLDER/sample-dpl/.env

az storage blob upload -f /TEST_FOLDER/sample-dpl/sample.tar.gz -c storage -n sample.tar.gz --account-name $AZURE_STORAGE_ACCOUNT --account-key $AZURE_STORAGE_KEY
