const express = require("express");
const app = express();

app.get("/", (req, res) => {
  res.send({ hello: "world_bernard_test - 12345" });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, function() {
  console.log(`App listening on port ${PORT}`);
});
